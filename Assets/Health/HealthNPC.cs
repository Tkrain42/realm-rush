﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthNPC : HealthBase {


    Animator animator;
    float lastHealth;

	protected override void Start () {
        base.Start();
        animator = GetComponent<Animator>();
        lastHealth = currentHealth;
        onHealthChanged += HealthChanged;
	}

    void HealthChanged(float c, float m)
    {
        if (currentHealth < lastHealth)
        {
            animator.SetTrigger("Hurt");
        }
        lastHealth = currentHealth;
    }

    protected override void onDeath()
    {
        base.onDeath();
        animator.SetTrigger("Die");
    }
}
