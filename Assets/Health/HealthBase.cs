﻿using System.Collections;
using System.Collections.Generic;
using Tkraindesigns.Extentions;
using UnityEngine;

public class HealthBase : MonoBehaviour {

    public delegate void OnHealthChanged(float current, float max);
    public event OnHealthChanged onHealthChanged;

    [SerializeField]
    protected float maxHealth;
    protected float currentHealth;

    public float CurrentHealth
    {
        get
        {
            return currentHealth;
        }
        set
        {
            if (!isAlive)
            {
                return;
            }
            currentHealth = value.Clamp(0.0f, maxHealth);
           
            if (onHealthChanged != null)
            {
                onHealthChanged(currentHealth, maxHealth);
            }
        }
    }

    public bool isAlive
    {
        get
        {
            return (currentHealth.Bool());
        }
    }

	// Use this for initialization
	protected virtual void Start () {
        currentHealth = maxHealth;
	}


    protected virtual void onDeath()
    {
        Invoke("Die", 5);
    }

    protected virtual void Die()
    {
        Destroy(gameObject);
    }
}
