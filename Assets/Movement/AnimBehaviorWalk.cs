﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimBehaviorWalk : AnimBehaviorBase {

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //npc.transform.rotation = Quaternion.Slerp(npc.transform.rotation, Quaternion.LookRotation(npc.desiredDirection),3*Time.deltaTime);
        if (!animator.applyRootMotion)
        {
            npc.transform.Translate(0, 0, npc.Speed * Time.deltaTime);
        }
        if (npc.desiredDirection.magnitude < 1)
        {
            animator.SetBool("Walking", false);
            npc.transform.position = npc.Destination;
        }
        
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
