﻿using System.Collections.Generic;
using Tkraindesigns.Extentions;

namespace Assets.TkrainDesigns.Collections
{
    public class RingBuffer<T>
    {
        T[] buffer;
        int length;
        int write = 0;
        int read = 0;
        bool carry = false;

        /// <summary>
        /// Instantiates ring buffer with a limit of bufferSize
        /// </summary>
        /// <param name="bufferSize"></param>
        public RingBuffer(int bufferSize)
        {
            buffer = new T[bufferSize];
            write = 0;
            read = 0;
            length = bufferSize;
        }

        /// <summary>
        /// Instantiates buffer with a default Length of 10
        /// </summary>
        public RingBuffer()
        {
            buffer = new T[10];
            write = 0;
            read = 0;
            length = 10;
            
        }

        /// <summary>
        /// Returns the size of the buffer (not the actual number of elements currently in the buffer, this is the maximum size;
        /// </summary>
        public int Length
        {
            get
            {
                return length;
            }
        }

        /// <summary>
        /// Returns the actual number of current elements in the ringbuffer.  (i.e. write-read)
        /// </summary>
        public int Count
        {
            get
            {
                if (read > write)
                {
                    return write+length - read;
                }
                else
                    if (read == write)
                    {
                        if (carry)
                        {
                            return Length;
                        }
                        else
                        {
                            return 0;
                        }
                    }
                return write - read;
            }
            
        }

        /// <summary>
        /// Attempts to add item to the buffer.  If the buffer is full (Count == Length), returns false.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Enqueue(T item)
        {
            if (Count == length)
            {
                return false;
            }
            buffer[write] = item;
            write = (write + 1) % length;
            if (write == read)
            {
                carry = true;
            }
            else
            {
                carry = false;
            }
            return true;
        }

        /// <summary>
        /// Returns the next item in the buffer, and removes it from the buffer. If the buffer is empty, returns the last item placed in the buffer.  
        /// TODO:  Throw buffer overflow exception
        /// </summary>
        /// <returns></returns>
        public T Dequeue()
        {
            T result = buffer[read];
            if (Count != 0)
            {
                carry = false;
                read = read.IncMod(length);
                return result;
            }
            return default(T);
            
        }

        /// <summary>
        /// Returns the next item in the buffer, but does not remove it from the buffer.  If the buffer is empty, returns the last item placed in the buffer
        /// TODO:  Throw buffer overflow exception
        /// </summary>
        /// <returns></returns>
        public T Peek()
        {
            return buffer[read];
        }

        /// <summary>
        /// Returns item [element] in the buffer, but does not remove it from the buffer.  If this extends past the buffer Count, unpredictable results may occur.
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public T Peek(int element)
        {
            
            return buffer[read.AddMod(element, length)];
        }

        /// <summary>
        /// Resets the buffer read and write counters to 0, effectively clearing the buffer.
        /// </summary>
        public void Clear()
        {
            read = 0;
            write = 0;
            carry = false;
        }

        /// <summary>
        /// Returns an array of the buffer with element [0] being the next in line. 
        /// </summary>
        /// <returns></returns>
        public T[] ToArray()
        {
            T[] r = new T[Count];
            for (int i = 0; i < Count; i++)
            {
                r[i] = Peek(i);
            }
            return r;
        }

        /// <summary>
        /// Returns a the buffer as a list, with the first element being the next element that would be Dequeued.
        /// </summary>
        /// <returns></returns>
        public List<T> ToList()
        {
            List<T> r = new List<T>();
            for (int i = 0; i < Count; i++)
            {
                r.Add(Peek(i));
            }
            
            return r;
        }

        /// <summary>
        /// Returns the buffer as a Queue, with the first element being the next element that would be Deqeued.
        /// </summary>
        /// <returns></returns>
        public Queue<T> ToQueue()
        {
            Queue<T> r = new Queue<T>();
            for (int i = 0; i < Count; i++)
            {
                r.Enqueue(Peek(i));
            }
            return r;
        }

        /// <summary>
        /// Changes the underlying array size.  Be aware that this will dispose of the original underlying array and replace it with a new one,
        /// which could lead to memory fragmentation 
        /// </summary>
        /// <param name="newDim"></param>
        /// <returns></returns>
        public bool Redim(int newDim)
        {
            if (newDim < Count)
            {
                return false;
            }
            Queue<T> queue = ToQueue();
            buffer = new T[newDim];
            read = 0;
            write = 0;
            carry = false;
            while (queue.Count > 0)
            {
                Enqueue(queue.Dequeue());
            }
            return true;
        }

        /// <summary>
        /// Returns true if item exists in the ring buffer.  We cheat here, and use List<> functionality to achieve the result.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Contains(T item)
        {
            return ToList().Contains(item);
        }

    }
}