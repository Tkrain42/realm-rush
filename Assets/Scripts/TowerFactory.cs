﻿using Assets.TkrainDesigns.Collections;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerFactory : MonoBehaviour {

    [SerializeField]
    Tower towerPrefab;
    [SerializeField]
    [Range(1,20)]
    int maxTowers = 5;
    RingBuffer<Tower> recycledTowers;
    List<Tower> activeTowers;

    public static TowerFactory instance;

    private void Awake()
    {
        instance = this;
    }

    // Use this for initialization
    void Start () {
        recycledTowers = new RingBuffer<Tower>(maxTowers);
        activeTowers = new List<Tower>(maxTowers);
	}


    public Tower PlaceTower(Waypoint waypoint)
    {
        if (activeTowers.Count >= maxTowers)
        {
            print("TowerFactory: There are already " + activeTowers.Count + " towers in the scene.  Tower not placed.");
            return null;
        }
        Tower tower = GetTower();
        tower.gameObject.SetActive(true);
        tower.transform.position = waypoint.transform.position;
        waypoint.containsObject = true;
        activeTowers.Add(tower);
        return tower;
    }

    public void RemoveTower(Tower tower)
    {
        FindObjectOfType<WorldMap>().getWaypointAt(tower.gridPosition).containsObject = false;
        tower.gameObject.SetActive(false);
        if (activeTowers.Contains(tower))
        {
            activeTowers.Remove(tower);
        }
        StowTower(tower);   
    }

    Tower GetTower()
    {
        if (recycledTowers.Count > 0)
        {
            return recycledTowers.Dequeue();
        }
        else
        {
            return Instantiate(towerPrefab);
        }
    }

    void StowTower(Tower tower)
    {
        recycledTowers.Enqueue(tower);
    }
}
