﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class ParticleWeapon : MonoBehaviour {

    [SerializeField]
    float damageMin = 1;
    float damageMax = 3;

    new ParticleSystem particleSystem;
    ParticleSystem.MainModule main;


    private void Start()
    {
        particleSystem = GetComponentInChildren<ParticleSystem>();
        main = particleSystem.main;
        
    }

    public float DamageMin
    {
        get
        {
            return damageMin;
        }
        set
        {
            float tmp = damageMax;
            damageMin = Mathf.Min(value, tmp);
            damageMax = Mathf.Max(value, tmp);
        }
    }

    public float DamageMax
    {
        get
        {
            return damageMax;
        }
        set
        {
            float tmp = damageMin;
            damageMin = Mathf.Min(value, tmp);
            damageMax = Mathf.Max(value, tmp);
        }
    }

    private void OnParticleCollision(GameObject other)
    {
        ////print("OnParticleCollision: "+other);
        //Enemy enemy = other.GetComponentInParent<Enemy>();
        //if (enemy)
        //{
        //    if (enemy.isAlive)
        //    {
        //        enemy.TakeDamage(damage);
        //    }

        //}
        HealthBase health = other.GetComponent<HealthBase>();
        if (health)
        {
            health.CurrentHealth -= Random.Range(damageMin, damageMax);
        }
    }

    public void OnFire()
    {
        if (!particleSystem.isPlaying)
        {
            particleSystem.Play();
        }

    }

    public void OnCeaseFire()
    {
        //print("Ceasing fire!");
        particleSystem.Stop();
    }
}
