﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(Waypoint))]
public class EditorLabeller : MonoBehaviour {
    TextMesh textMesh;
    Waypoint waypoint;
    [SerializeField]
    GameObject label;

    private void Awake()
    {
        if (label)
        {
            textMesh = label.GetComponent<TextMesh>();
        }
        
        waypoint = GetComponent<Waypoint>();
    }
    // Use this for initialization
	
	// Update is called once per frame
	void Update () {
        AdjustLabels();
        ColorStartAndEndPoints();
	}

    private void AdjustLabels()
    {
        string labelText = waypoint.gridX + "," + waypoint.gridY;
        setTextMeshText(labelText);
        gameObject.name = waypoint.terrainName+" (" + labelText + ")";
    }

    private void ColorStartAndEndPoints()
    {
        WorldMap world = FindObjectOfType<WorldMap>();
        if (world)
        {
            if (world.StartPosition.gridPosition == waypoint.gridPosition)
            {
                setTextMeshText("Start");
            }
            if (world.EndPosition.gridPosition == waypoint.gridPosition)
            {
                setTextMeshText("Finish");
            }
        }
    }

    void setTextMeshText(string text)
    {
        if (textMesh)
        {
            textMesh.text = text;
        }
    }
}
