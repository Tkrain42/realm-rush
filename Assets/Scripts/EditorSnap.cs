﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[SelectionBase]
[RequireComponent(typeof(Gridlike))]
public class EditorSnap : MonoBehaviour {

    Gridlike gridlike
    {
        get
        {
            return GetComponent<Gridlike>();
        }
    }



    // Update is called once per frame
    void Update ()
    {
        ClampPosition();
    }



    private void ClampPosition()
    {
        transform.position = gridlike.idealWorldPosition;
    }

}
