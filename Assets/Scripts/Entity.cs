﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : Gridlike {


    HealthBase health;


    protected virtual void Start()
    {
        print("Entity Start");
        health = GetComponent<HealthBase>();
    }

    public bool isAlive
    {
        get
        {
            if (health)
            {
                return health.isAlive;
            }
            else
            {
                health = GetComponent<HealthBase>();
                return health.isAlive;
            }
            
        }
    }

    public virtual float queueTime()
    {
        return 0.0f;
    }

    public virtual IEnumerator StartTurn()
    {
        yield return new WaitForEndOfFrame();
    }

}
