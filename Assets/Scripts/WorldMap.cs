﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tkraindesigns.Collections;

public class WorldMap : MonoBehaviour {


    [SerializeField]
    private Waypoint startPosition;
    [SerializeField]
    private Waypoint endPosition;

    [SerializeField] bool hexagonal=false;
    bool mapIsLoaded;
    public bool MapIsLoaded
    {
        get
        {
            return mapIsLoaded;
        }
    }

    public delegate void MapLoaded();
    public static event MapLoaded OnMapLoaded;

    Dictionary<Vector2Int, Waypoint> grid = new Dictionary<Vector2Int, Waypoint>();

    Vector2Int[] directionsVertical =
    {
        Vector2Int.up,
        Vector2Int.down,
        Vector2Int.left,
        Vector2Int.right
    };
    Vector2Int[] directionsHorizontal=
    {
        Vector2Int.left,
        Vector2Int.right,
        Vector2Int.up,
        Vector2Int.down
    };

    Vector2Int[] directionsHexEven =
    {
        Vector2Int.left,
        Vector2Int.right,
        Vector2Int.up,
        Vector2Int.down,
        new Vector2Int(-1,+1),
        new Vector2Int(+1,+1)
    };

    Vector2Int[] directionsHexOdd =
    {   
        Vector2Int.left,
        Vector2Int.right,
        Vector2Int.up,
        Vector2Int.down,
        new Vector2Int(-1,-1),
        new Vector2Int(+1,-1)
    };

    public Waypoint StartPosition
    {
        get
        {
            return startPosition;
        }
    }

    public Waypoint EndPosition
    {
        get
        {
            return endPosition;
        }
    }

	// Use this for initialization
	void Start ()
    {
        LoadBlocks();
        startPosition.SetColor(Color.blue);
        endPosition.SetColor(Color.red);
    }

    private void LoadBlocks()
    {
        var waypoints = FindObjectsOfType<Waypoint>();
        foreach (Waypoint waypoint in waypoints)
        {
            if (!grid.ContainsKey(waypoint.gridPosition))
            {
                grid.Add(waypoint.gridPosition, waypoint);
                waypoint.SetColor(Color.yellow);
            }
            else
            {
                waypoint.SetColor(Color.black);
                //waypoint.transform.position = new Vector3(waypoint.transform.position.x, 1, waypoint.transform.position.z);
                grid[waypoint.gridPosition].SetColor(Color.black);
                Debug.LogWarning("Not loading waypoint " + waypoint.gridPosition + " due to overlap error.");
            }

        }
        mapIsLoaded = true;
        if (OnMapLoaded != null)
        {
            OnMapLoaded();
        }
    }

    public List<Waypoint> ChartCourse(Waypoint start, Waypoint finish)
    {
        //Dictionary<Vector2Int, Waypoint> map = BreadthFirstSearch(start, finish);
        Dictionary<Vector2Int, Waypoint> map = DykstraSearch(start, finish);
        Stack<Waypoint> stack = new Stack<Waypoint>();
        Waypoint test = finish;
        if (!map.ContainsKey(finish.gridPosition))
        {
           // print("Can't find path.");
            return null;
        }
        //print("Tracing from " + test.name);
        
        while (!(test.gridPosition == start.gridPosition))
        {
            stack.Push(test);
            Waypoint temp = map[test.gridPosition];
            test = temp;
           // print("Adding " + test.name + " to path.");
        }
        return new List<Waypoint>(stack.ToArray());
    }

    public List<Waypoint> ChartCourse()
    {
        return ChartCourse(StartPosition, EndPosition);
    }

    public List<Waypoint> ChartCourse(Waypoint start)
    {
        return ChartCourse(start, EndPosition);
    }



    public Dictionary<Vector2Int, Waypoint> DykstraSearch(Waypoint start, Waypoint finish)
    {
        Dictionary<Vector2Int, Waypoint> result = new Dictionary<Vector2Int, Waypoint>();
        PriorityQueue<Waypoint> queue = new PriorityQueue<Waypoint>();
        Vector2Int testPosition;
        float maxBlockingDistance = start.distanceFrom(finish) / 2.0f;
        queue.Enqueue(start,0);
                Waypoint test;
        int directionCount = 4;
        if (hexagonal)
            directionCount = 6;
        float currentPriority = 0;
        while (queue.Count > 0)
        {
            test = queue.Dequeue( out currentPriority);
            for (int i = 0; i < directionCount; i++)
            {
                if (!hexagonal)
                {
                    if (ClosestRouteIsVertical(test.gridPosition, finish.gridPosition))
                    {
                        testPosition = test.gridPosition + directionsVertical[i];
                    }
                    else
                    {
                        testPosition = test.gridPosition + directionsHorizontal[i];
                    }
                }
                else
                {
                    if (test.gridX % 2 == 0)
                    {
                        testPosition = test.gridPosition + directionsHexOdd[i];
                    }
                    else
                    {
                        testPosition = test.gridPosition + directionsHexEven[i];
                    }
                }

                if (grid.ContainsKey(testPosition))
                {
                    if (!result.ContainsKey(testPosition))
                    {
                        if (!testLocationForObstacles(grid[testPosition], start, maxBlockingDistance)) //ignore positions with current enemy
                        {
                            float newPriority = currentPriority + test.EdgeCost(grid[testPosition]);
                            queue.Enqueue(grid[testPosition], newPriority);
                            result.Add(testPosition, test);
                            if (testPosition == finish.gridPosition)
                            {
                                return result;
                            }
                        }
                    } else
                    {
                        //print("Location " + grid[testPosition].name + " is occupied, not adding to path.");
                    }
                }
            }
        }
        return result;
    }


    bool testLocationForObstacles(Waypoint testWaypoint, Waypoint destination, float whoCaresDistance)
    {
        float testDistance = testWaypoint.distanceFrom(destination);
        if (testDistance > whoCaresDistance)
        {
            //print(testDistance + ">" + whoCaresDistance);
            return false;
        }
        else
        {
            //print(testDistance + "<" + whoCaresDistance);
            return testWaypoint.containsObject;
        }
    }

    public Waypoint getWaypointAt(Vector2Int location)
    {
        return grid[location];
    }
    
    bool ClosestRouteIsVertical(Vector2Int first, Vector2Int second)
    {
        int deltaX = Mathf.Abs(first.x - second.x);
        int deltaY = Mathf.Abs(first.y- second.y);
        return (deltaX < deltaY);
    }

}
