﻿using System.Collections;
using System.Collections.Generic;
using Tkraindesigns.Collections;
using UnityEngine;

public class EnemyCoordinator : MonoBehaviour {

    PriorityQueue<Entity> queue = new PriorityQueue<Entity>();
    float lastPriority = 0f;

    private void Awake()
    {
        WorldMap.OnMapLoaded += OnMapLoaded;
    }

    void OnMapLoaded()
    {
        StartCoroutine(ProcessQueue());
    }

    public void Enqueue(Entity entity)
    {
        print("Adding " + entity.name + " to Coordinator");
        lastPriority += .1f;
        queue.Enqueue(entity, lastPriority);

    }

    IEnumerator ProcessQueue()
    {
        while (true)
        {
            if (queue.Count > 0)
            {
                Entity entity = queue.Dequeue(out lastPriority);
                //if (entity) print(entity.name);
                if(entity && entity.isAlive)
                {
                    string eName = entity.name;
                    print("Entity " + eName + " is making a move");
                    yield return StartCoroutine(entity.StartTurn());
                    if (entity)
                    {
                        print("Entity " + eName + " has finished his move");
                        lastPriority += entity.queueTime();
                        queue.Enqueue(entity, lastPriority);
                    }
                    else
                    {
                        print("Entity " + eName + " has died.");
                    }

                    print("EnemyCoordinator Count: " + queue.Count);
                }
                else
                {
                    yield return new WaitForEndOfFrame();
                }
            }
        }
    }

}
