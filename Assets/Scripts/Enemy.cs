﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Enemy : Entity
{

    List<Waypoint> path;

    [Range(.1f, 60f)]
    [Tooltip("How long after finishing the movement before the Enemy makes another move.")]
    [SerializeField]
    float secondsBetweenMoves = .5f;
    [Range(.1f, 60f)]
    [Tooltip("How long the actual move takes.")]
    [SerializeField]
    float secondsToMove = .5f;
    [Range(1, 20)]
    [SerializeField]
    int forceRecalcAfter = 5;
    WorldMap worldmap;
    Waypoint targetWaypoint = null;
    Animator animator;
    NPCMovement movement;
    

    public float SecondsBetweenMoves
    {
        get
        {
            return secondsBetweenMoves;
        }
    }


    
    Waypoint currentWaypoint
    {
        get
        {
            return worldmap.getWaypointAt(gridPosition);
        }
    }
    // Use this for initialization

    private void OnEnable()
    {
        WorldMap.OnMapLoaded += DelayedStart;
    }

    private void OnDisable()
    {
        WorldMap.OnMapLoaded -= DelayedStart;
    }

    protected override void Start()
    {
        print("Enemy Start");
        base.Start();
        worldmap = FindObjectOfType<WorldMap>();
        animator = GetComponent<Animator>();
        if (!animator)
        {
            animator = GetComponentInChildren<Animator>();
        }
        movement = GetComponent<NPCMovement>();
        if (!movement)
        {
            movement = GetComponentInChildren<NPCMovement>();
        }
        EnemyCoordinator enemyCoordinator = FindObjectOfType<EnemyCoordinator>();
        // enemyCoordinator.Enqueue(this);
        StartCoroutine(StartTurn());
    }

    void SetWalking(bool isWalking)
    {
        if (animator)
        {
            animator.SetBool("Walking", isWalking);
        }
    }

    void Attack()
    {
        if(animator)
        {
            animator.SetTrigger("Attack");
        }
    }

    void Stifled()
    {
        if(animator)
        {
            animator.SetTrigger("Stifled");
        }
    }

    public override float queueTime()
    {
        if (!isAlive)
        {
            return -1;
        } else if (currentWaypoint == worldmap.EndPosition)
        {
            return -1;
        }
        else if (pathCount > 1)
        {
            return currentWaypoint.EdgeCost(path[1])*SecondsBetweenMoves;
        }
        return currentWaypoint.TravelCost;
    }

    int pathCount
    {
        get
        {
            if (path == null)
            {
                return 0;
            }
            else
            {
                return path.Count;
            }
        }
    }

    override public IEnumerator StartTurn()
    {
        while (true)
        {

            if (currentWaypoint.gridPosition == worldmap.EndPosition.gridPosition)
            {
                Attack();
                Invoke("Die", secondsBetweenMoves * 2);
                yield return new WaitForEndOfFrame();
            }
            else if (pathCount > 1 && (!path[1].containsObject))
            {
                path.Remove(path[0]);
                yield return StartCoroutine(FollowPath());
            }

            else
            {
                path = worldmap.ChartCourse(currentWaypoint);
                if (path != null)
                {
                    if (path[0].containsObject)
                    {
                        Stifled();
                        yield return new WaitForEndOfFrame();
                    }
                    else
                    {
                        yield return StartCoroutine(FollowPath());

                    }
                }
            }
        }
    }

    private void DelayedStart()
    {
        if (currentWaypoint.gridPosition == worldmap.EndPosition.gridPosition)
        {
            //print(name + " has reached the destination.");
            Attack();
            Invoke("Die", secondsBetweenMoves*2);
        }
        else 
        {
            //print(name + "is plotting its next move.");
            currentWaypoint.containsObject = true;
            path = worldmap.ChartCourse(currentWaypoint);
            if (path != null)
            {
                StartCoroutine(FollowPath());
            }
            else
            {
                Invoke("DelayedStart", secondsBetweenMoves);
            }
        }
    }

    // Update is called once per frame

    IEnumerator FollowPath()
    {
        //print("Starting Patrol");
        int counter = 0;
        float moveCost = 1;
        Waypoint waypoint = path[0];
        while(counter==0)
        {
            counter++;
            if (counter > forceRecalcAfter)
            {
                break;
            }
            if (waypoint.containsObject)
            {
                int attempts = 0;
                while(waypoint.containsObject && attempts < 10)
                {
                    attempts++;
                    Stifled();
                    yield return new WaitForSeconds(secondsBetweenMoves + secondsToMove);
                }
                if (waypoint.containsObject)
                {
                    break;
                }
            }
            currentWaypoint.containsObject = false;
            waypoint.containsObject = true;
            targetWaypoint = waypoint;
            Vector3 targetDir = waypoint.transform.position - transform.position;
            Vector3 newRot = Vector3.RotateTowards(transform.forward, targetDir.normalized, Mathf.PI, 0.0f);
            transform.rotation = Quaternion.LookRotation(newRot);
            moveCost = waypoint.EdgeCost(currentWaypoint);
            yield return StartCoroutine(Move(transform.position, waypoint.transform.position));
            if (isAlive)
            {
                currentWaypoint.containsObject = true;
            }
            //yield return new WaitForSeconds(secondsBetweenMoves * moveCost);
        }
        //Invoke("DelayedStart", secondsBetweenMoves); //Only reached if you found the destination.
    }

    IEnumerator Move(Vector3 start, Vector3 finish)
    {
        float slice = 1 / Time.deltaTime;
        float movementDelay = (secondsToMove) / slice;
        //SetWalking(true);
        //for (int i = 0; i < (int)slice; i++)
        //{
        //    if (!isAlive)
        //    {
        //        break;
        //    }
        //    transform.position = Vector3.Lerp(start, finish, (float)i / slice);
        //    yield return new WaitForSeconds(movementDelay);
        //}
        //SetWalking(false);
        transform.rotation = Quaternion.LookRotation(finish - transform.position);
        movement.Destination = finish;
        while (isAlive && movement.desiredDirection.magnitude > 1)
        {
            yield return new WaitForSeconds(movementDelay);
        }
        if (isAlive)
        {
            transform.position = finish;
        }
    }

    void Die()
    {
        currentWaypoint.containsObject = false;
        currentWaypoint.SetColor(Color.red);
        Destroy(gameObject);
    }

    void Slain()
    {
        Destroy(gameObject);
    }


}
