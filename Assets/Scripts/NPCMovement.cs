﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class NPCMovement : MonoBehaviour {

    Animator animator;
    Vector3 destination;
    [SerializeField]
    float speed = 2.0f;

    public float Speed
    {
        get
        {
            return speed;
        }
    }

    public Vector3 Destination
    {
        get
        {
            return destination;
        }

        set
        {
            destination = value;
            if (animator)
            {
                animator.SetBool("Walking", true);
            }
            else
            {
                transform.position = value;
            }
        }
    }


    public Vector3 desiredDirection
    {
        get
        {
            return destination - transform.position;
        }
    }

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
        if (!animator)
        {
            Debug.LogWarning(name + ": No Animator found.");
        }
	}
	

}
