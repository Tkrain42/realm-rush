﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : Gridlike {

    [SerializeField]
    float travelCost = 1;
    [SerializeField]
    List<GameObject> colorMeshes;
    [SerializeField]
    string _terrainName = "Field";

    public string terrainName
    {
        get
        {
            return _terrainName;
        }
    }

    private bool _containsObject = false;
    public bool containsObject
    {
        get
        {
            return _containsObject;
        }
        set
        {
            if (value)
            {
                if (!_containsObject)
                {
                    StartCoroutine(SanityCheck());
                }
            }
            _containsObject = value;
            if (_containsObject)
            {
                //SetColor(Color.blue);
                
            }
            else
            {
                //SetColor(Color.cyan);
            }
        }
    }

    public float TravelCost
    {
        get
        {
            return travelCost;
        }
    }

    public float EdgeCost(Waypoint from)
    {
        float rawEdgeCost = from.TravelCost + travelCost;
        return rawEdgeCost / 2f;
    }

    IEnumerator SanityCheck()
    {
        yield return new WaitForSeconds(1.0f);
        while (true)
        {
            bool foundObject = false;
            var enemies = FindObjectsOfType<Enemy>();
            foreach (Enemy enemy in enemies)
            {
                if(enemy)
                if (enemy.gridPosition == gridPosition)
                {
                    foundObject = true;
                }
                yield return new WaitForEndOfFrame();
            }
            var towers = FindObjectsOfType<Tower>();
            foreach(Tower tower in towers)
            {
                if (tower.gridPosition == gridPosition)
                {
                    foundObject = true;
                }
                yield return new WaitForEndOfFrame();
            }
            if(!foundObject)
            {
                _containsObject = false;
                //SetColor(Color.cyan);
            }
            else
            {
                yield return new WaitForSeconds(1.0f);
            }
        }
    }

    public void SetColor(Color color)
    {
        if (colorMeshes.Count > 0)
        {
            foreach(GameObject g in colorMeshes)
            {
                var meshRenderer = g.GetComponent<MeshRenderer>();
                if (meshRenderer)
                {
                    meshRenderer.material.color = color;
                }
            }
        }      
    }




}


