﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemySpawner : Gridlike {

    [SerializeField]
    GameObject enemyToSpawn;
    [SerializeField]
    int maxActiveSpawns = 5;
    [SerializeField]
    [Range(.5f,30)]
    float spawnFrequency;
    [SerializeField]
    bool randomSpawnFrequency = true;
    ParticleSystem particle;
    WorldMap worldmap;
    Waypoint waypoint;

    // Use this for initialization

    private void OnEnable()
    {
        WorldMap.OnMapLoaded += QueueSpawn;
    }



    void Start () {
        worldmap = FindObjectOfType<WorldMap>();
        particle = GetComponent<ParticleSystem>();
        if (worldmap.MapIsLoaded)
        {
            QueueSpawn();
        }
	}

    void QueueSpawn()
    {
        waypoint = worldmap.getWaypointAt(gridPosition);
        Invoke("SpawnEnemy", nextSpawnTime);
    }

	void SpawnEnemy()
    {
        var enemies = GetComponentsInChildren<Enemy>();
        if (!waypoint.containsObject)
        {
            if (enemies.Length <= maxActiveSpawns)
            {
                if (particle)
                {
                    particle.Play();
                }
                Instantiate(enemyToSpawn, transform.position, Quaternion.identity, transform);
                waypoint.containsObject = true;
            } 
        }

        //Instantiate(enemyToSpawn, transform.position, Quaternion.identity);
        Invoke("SpawnEnemy", nextSpawnTime);
    }

    float nextSpawnTime
    {
        get
        {
            if (randomSpawnFrequency)
            {
                return (spawnFrequency / 2) + Random.Range(0, spawnFrequency);
            }
            else
            {
                return spawnFrequency;
            }
        }
    }
}
