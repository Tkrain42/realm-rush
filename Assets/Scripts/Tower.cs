﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Layer
{
    Enemy = 8,
    Player = 9,
    Obstacle = 10,
    Waypoint = 11,
    RaycastEndStop = -1
}

public class Tower : Entity {

    [SerializeField] Transform pivot;

    [Range(.1f,10)]
    [SerializeField] float rotationSpeed = 1f;
    [Range(1, 100)]
    [SerializeField]
    float shotRange = 50;
    [Range(1,20)]
    [SerializeField] float damageMin = 1;
    [SerializeField] float damageMax = 3;

    public Layer[] layerPriorities = {
        Layer.Obstacle,
        Layer.Enemy
    };

    Enemy currentTarget = null;
    ParticleWeapon particleWeapon;

    protected override void Start () {
        base.Start();
        MarkWaypointOccupied();
        particleWeapon = GetComponentInChildren<ParticleWeapon>();
        if (particleWeapon)
        {
            particleWeapon.DamageMin = damageMin;
            particleWeapon.DamageMax = damageMax;
            
        }
        FindObjectOfType<EnemyCoordinator>().Enqueue(this);
	}
	
    void MarkWaypointOccupied()
    {
        var waypoints = FindObjectsOfType<Waypoint>();
        foreach(Waypoint waypoint in waypoints)
        {
            if (gridPosition == waypoint.gridPosition)
            {
                waypoint.containsObject = true;
            }
        }
    }

    public override float queueTime()
    {
        return .25f;
    }

    public override IEnumerator StartTurn()
    {
        if (targetDistance() < shotRange)
        {
            if (particleWeapon)
            {
                particleWeapon.OnFire();
                yield return new WaitForSeconds(1);
                particleWeapon.OnCeaseFire();
            }
        }
        else
        yield return base.StartTurn();
    }

    Enemy findClosestEnemy()
    {
        var Enemies = FindObjectsOfType<Enemy>();
        Enemy result = null;
        float distance = shotRange;
        foreach(Enemy enemy in Enemies)
        {
            if (enemy.isAlive && (enemy.distanceFrom(transform.position) < distance))
            {
                if (RaycastForObstacle(enemy.transform.position))
                {
                    //print("Ignoring " + enemy.name + " because there is a wall blocking view.");
                }
                else
                {
                    distance = enemy.distanceFrom(transform.position);
                    result = enemy;
                }
            }
        }
        return result;
    }


    float targetDistance()
    {
        if (!currentTarget)
            return Mathf.Infinity;
        Vector3 delta = transform.position - currentTarget.transform.position;
        return Mathf.Abs(delta.magnitude);
    }

    // Update is called once per frame
    void Update () {
        if (currentTarget)
        {
            Vector3 dir = currentTarget.transform.position - transform.position;
            Vector3 newRot = Vector3.RotateTowards(pivot.transform.forward, dir.normalized, rotationSpeed*Time.deltaTime, 0.0f);
            pivot.transform.rotation = Quaternion.LookRotation(newRot);
        }
        currentTarget = findClosestEnemy();

	}

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, shotRange);
        Gizmos.color = Color.white;
        if(targetDistance()<200)
        Gizmos.DrawWireSphere(transform.position, targetDistance());
    }

    bool RaycastForObstacle(Vector3 target)
    {
        int layerMask = 1 << (int)Layer.Obstacle; // See Unity docs for mask formation
        Ray ray = new Ray(transform.position, target-transform.position);

        RaycastHit hit; // used as an out parameter
        bool hasHit = Physics.Raycast(ray, out hit, (target-transform.position).magnitude, layerMask);
        return hasHit;
    }

}
