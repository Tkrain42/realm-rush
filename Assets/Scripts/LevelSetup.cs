﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// A repository for details that should be "global" in a given level.  
// There should only be one of these in a scene.
public class LevelSetup : MonoBehaviour {

    [SerializeField]
    int gridSize = 10;
    [SerializeField]
    bool _hexagonal = false;

    public int GridSize
    {
        get
        {
            return gridSize;
        }
    }

    public bool Hexagonal
    {
        get
        {
            return _hexagonal;
        }
    }

}
