﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
public class ColorTile : MonoBehaviour {

    [SerializeField] Material idleMaterial;
    [SerializeField] Material errorMaterial;
    [SerializeField] Material highlightMaterial;

    MeshRenderer meshRenderer;

    private void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        meshRenderer.material = idleMaterial;
    }

    public void SetIdle()
    {
        meshRenderer.material = idleMaterial;
    }

    public void SetError()
    {
        meshRenderer.material = errorMaterial;
    }

    public void SetHighlight()
    {
        meshRenderer.material = highlightMaterial;
    }


}
