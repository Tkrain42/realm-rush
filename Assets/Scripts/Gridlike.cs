﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gridlike : MonoBehaviour {
    
    const float xFactor = .834f;
    const float yFactor = .956f;

    //float hexGridSizeX = 8.34f;
    //float hexGridSizeZ = 9.56f;
    LevelSetup _levelsetup;
    int _gridSize;
    bool _hex;
    float hexX;
    float hexY;
    bool gs_init = false;
    bool hex_init = false;
    

    LevelSetup levelSetup
    {
        get
        {
            if (!_levelsetup)
            {
                //print("Finding level setup");
                _levelsetup = FindObjectOfType<LevelSetup>();
            }
            return _levelsetup;
        }
    }

    int gridSize
    {
        get
        {
            if (gs_init)
            {
                return _gridSize;
            }
            if (levelSetup)
            {
                _gridSize = levelSetup.GridSize;
                gs_init = true;
                return _gridSize;
            }
            else
            {
                return 10;
            }
        }
    }

    bool hexagon
    {
        get
        {
            if (hex_init)
            {
                return _hex;
            }
            if (levelSetup)
            {
                _hex = levelSetup.Hexagonal;
                hex_init = true;
                return _hex;
            } else
            {
                return false;
            }
        }
    } 

    float hexGridSizeX
    {
        get
        {
            return gridSize * xFactor;
        }
    }

    float hexGridSizeY
    {
        get
        {
            return gridSize * yFactor;
        }
    }

    public Vector2Int gridPosition
    {
        get
        {
            return new Vector2Int(gridX, gridY);
        }
    }

    public Vector3 idealWorldPosition
    {
        get
        {
            if (hexagon)
            {
                
                float offset = hexGridSizeY* Xeven*.5f;
                return new Vector3((gridX * hexGridSizeX), 0, gridY * hexGridSizeY+offset);
            }
            else
            {
                return new Vector3(gridX * gridSize, 0, gridY * gridSize);
            }
            
        }
    }

    // Use this for initialization
    public int gridX
    {
        get
        {
            if (hexagon)
            {
                return Mathf.RoundToInt(transform.position.x / (float)hexGridSizeX); 
            }
            else
            {
                return Mathf.RoundToInt(transform.position.x / ((float)gridSize));
            }
        }
    }

    int Xeven
    {
        get
        {
            return Mathf.Abs(gridX%2);
        }
    }


    public int gridY
    {
        get
        {
            if (hexagon)
            {
                float offset = hexGridSizeY * Xeven * .45f;
                float zed=transform.position.z-offset;
                return Mathf.RoundToInt(zed / (float)hexGridSizeY);
            }
            else
            {
                return Mathf.RoundToInt(transform.position.z / (float)gridSize); 
            }
        }
    }

    public float distanceFrom(Vector3 location)
    {
        return Mathf.Abs(Vector3.Distance(location, transform.position));
    }

    public float distanceFrom(Gridlike location)
    {
        return Mathf.Abs(Vector3.Distance(location.transform.position, transform.position));
    }

}
