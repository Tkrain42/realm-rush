# Realm Rush #

Welcome to my implementation of the code for Realm Rush, based on the [Complete Unity Developer Course Version 2](https://www.udemy.com/unitycourse2/learn/v4/content)
taught by [Ben Tristem](https://twitter.com/BenTristem) and [Rick Davidson](https://twitter.com/@rickdme). 
In addition to the course, you can join the discussion forums at [Gamedev.tv](http://www.gamedev.tv) 

Realm Rush will serve as a prototype for a grid based tower defense game.  The added flavor in this version compared to the course version is that I've implemented the solution to work on either normal square 
grids or hex grids.

### What is this repository for? ###

This repository is best served as code examples.  The project as represented in this backup is NOT complete.  This is by design.  Purchased assets from the Asset store (even free ones) are the
intellectual property of their authors.  While there is an implicit license to use and distribute parts of the assets, this doesn't mean those assets can be released to the general public.  
there's no real need to take up extra space in a repository (often LFS storage, so $$) when the assets are available to me to restore within the editor from the asset store.  My .gitignore will always 
contain the subdirectories of 3rd party assets.  I will endeavor to document any 3rd party assets used in any projects.  If you are cloning this repo with the idea of recreating the game exactly, you'll have
to aquire the assets and rebuild it.  In some cases, this will just be a matter of replacing a material.  In others, entire Prefabs may need to be tweaked and edited.  

### How do I get set up? ###

In it's current state, this game is just a tech demo.  You can clone it, and in all likelyhood it will run out of the box.  The only external assets used at this time are Unity's ProBuilder and
[Epyphany games PBR Materials](https://assetstore.unity.com/publishers/18282).  If you use neither, this should still run as I've exported the Hex tiles created in ProBuilder to obj files.

For more information on the underlying classes, please see the [wiki](https://bitbucket.org/Tkrain42/realm-rush/wiki/Home)

### Dependencies ###
* [Epyphany games PBR Materials](https://assetstore.unity.com/publishers/18282)
* [ProBuilder](https://assetstore.unity.com/packages/tools/modeling/probuilder-111418)

## Contact
* [Twitter](https://twitter.com/tkrain42)
* [Email](mailto:mnemoth42@gmail.com)

